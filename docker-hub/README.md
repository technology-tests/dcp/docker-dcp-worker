# Intro

## Quick start

```sh
docker run \
    --env account="0x8614aaf51331f7f874c5b5e1b63377e241ed01a8" \
    jonstonchan/dcp-worker:latest
```


## DCP

- [Official homepage](https://www.dcp.dev/)
- [Official documentation](https://docs.dcp.dev/)
- [Official web worker][official web worker]
- [Official Docker worker](https://hub.docker.com/r/distributivenetwork/dcp-worker)
    - at the time of writing, several versions behind

Distributive Compute Protocol (DCP) is a community-based parallel computing platform
where anyone can join for free.

It is built on top of web technologies such as JavaScript and WebGL.
WebGPU support is planned for the future.


## This repository

Provides a non-interactive Docker image intended for headless Linux clients
to earn credits ("DCC") to use on DCP.

It is recommended that non-headless clients use the [official web worker][official web worker].
The official web worker supports x86, x86-64, and ARM architectures.

### Contributing

- [Source code](https://gitlab.com/technology-tests/dcp/docker-dcp-worker)


[official web worker]: https://dcp.work/
