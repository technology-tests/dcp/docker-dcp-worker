#!/usr/bin/env bash

# Create a new id.keystore to ensure that this host's ID is unique
if [ ! -f "/opt/dcp/.dcp/id.keystore" ]; then
    sudo --user dcp \
        /opt/dcp/bin/mkad new id -p ""
fi

service xinetd restart

sudo --user dcp \
    bin/dcp-worker \
        --verbose \
        --reportInterval 10 \
        --output console \
        --payment-address="${account:-0x8614aaf51331f7f874c5b5e1b63377e241ed01a8}"
