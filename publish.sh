#!/usr/bin/env bash

function check_contents() {
    contents="$1"
    error_message="$2"

    if [ -z "$contents" ]; then
        echo "$error_message"
        exit 1
    fi
}

source .env.build

check_contents "$DOCKER_HUB_USERNAME" "Environment variable DOCKER_HUB_USERNAME required"
docker_hub_repo="$DOCKER_HUB_USERNAME/dcp-worker"
docker_hub_tag="worker-${DCP_WORKER_VERSION}-util-${DCP_UTIL_VERSION}"

check_contents "$DOCKER_HUB_API_KEY" "Environment variable DOCKER_HUB_API_KEY required"
sudo docker login -u "$DOCKER_HUB_USERNAME" -p "$DOCKER_HUB_API_KEY" || exit 1

check_contents "$DCP_UTIL_VERSION" "Environment variable DCP_UTIL_VERSION required"
check_contents "$DCP_WORKER_VERSION" "Environment variable DCP_WORKER_VERSION required"
sudo docker build \
    -f Dockerfile \
    --build-arg DCP_UTIL_VERSION \
    --build-arg DCP_WORKER_VERSION \
    -t "${docker_hub_repo}:${docker_hub_tag}" \
    -t "${docker_hub_repo}:latest" \
    .

sudo docker push "${docker_hub_repo}:${docker_hub_tag}"
sudo docker push "${docker_hub_repo}:latest"
