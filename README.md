# Intro

## DCP

- [Official homepage](https://www.dcp.dev/)
- [Official documentation](https://docs.dcp.dev/)
- [Official web worker][official web worker]
- [Official Docker worker](https://hub.docker.com/r/distributivenetwork/dcp-worker)
    - at the time of writing, several versions behind

Distributive Compute Protocol (DCP) is a community-based parallel computing platform
where anyone can join for free.

It is built on top of web technologies such as JavaScript and WebGL.
WebGPU support is planned for the future.


## This repository

Provides a non-interactive Docker image intended for headless Linux clients
to earn credits ("DCC") to use on DCP. The setup is based on the
[Ubuntu 20.04 DCP worker installation guide](https://archive.distributed.computer/releases/linux/ubuntu-20.04/README.html).

It is recommended that non-headless clients use the [official web worker][official web worker].
The official web worker supports x86, x86-64, and ARM architectures.


# Development

At the time of writing, only x86 and x86-64 architectures are supported.
ARM architectures will be able to build the image,
but result in a runtime error for the evaluator.

## Docker Compose

- `./start.sh`
    -  builds and tags the image, then starts a container


## Docker

```sh
source .env.build

sudo docker build \
    -f Dockerfile \
    --build-arg DCP_UTIL_VERSION \
    --build-arg DCP_WORKER_VERSION \
    -t docker-dcp-worker:local .

sudo docker run --name docker-dcp-worker docker-dcp-worker:local || \
sudo docker start -a docker-dcp-worker

sudo docker stop docker-dcp-worker
```


# Publish to Docker Hub

- `./publish.sh`


[official web worker]: https://dcp.work/
