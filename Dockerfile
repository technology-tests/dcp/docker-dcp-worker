FROM node:20.5.0-slim

RUN apt-get update \
 && apt-get install -y curl sudo xinetd \
 && rm -rf /var/cache/apt/archives /var/lib/apt/lists

RUN curl 'https://archive.distributed.computer/releases/linux/ubuntu-20.04/evaluator-v8-latest.tar.gz' \
  | tar -xzf - -C /

COPY config/dcp-config.js /opt/dcp/.dcp/
COPY config/dcp-evaluator /etc/xinetd.d/

WORKDIR /opt/dcp

ARG DCP_UTIL_VERSION
ARG DCP_WORKER_VERSION

RUN npm install \
    --global \
    --prefix=/opt/dcp \
    dcp-util@"$DCP_UTIL_VERSION" \
    dcp-worker@"$DCP_WORKER_VERSION"

COPY config/startup.sh bin/

RUN useradd -U -d /opt/dcp -s /usr/sbin/nologin dcp \
 && chown -R dcp:dcp /opt/dcp/

CMD bin/startup.sh
